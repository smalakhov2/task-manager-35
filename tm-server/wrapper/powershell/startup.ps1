write-host  "STARTING SERVER..."

if (Test-Path "server.pid") {
  write-host  "Application already started!"
  Wait-Event -Timeout 5
  exit 1
}

New-Item -Name log -ItemType Directory -Force
Remove-Item log/* -Recurse
$process = Start-Process javaw -ArgumentList "-jar ./tm-server.jar" -RedirectStandardOutput log/server.log -RedirectStandardError log/err.log -PassThru
$process.id | Out-File server.pid
write-host  "OK"
Wait-Event -Timeout 5