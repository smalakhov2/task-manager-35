package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.dto.AbstractEntityDto;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.repository.dto.AbstractDtoRepository;

import java.util.Collection;
import java.util.List;

public interface IService<E extends AbstractEntityDto, R extends AbstractDtoRepository<E>> {

    void save(@Nullable final E entity);

    void saveAll(@Nullable final Collection<E> entities);

    @Transactional
    public void saveAll(@Nullable final E... entities);

    @NotNull
    List<E> findAllDto();

    @Nullable
    E findOneDtoById(@NotNull final String id) throws EmptyIdException;

    boolean existById(@Nullable final String id) throws EmptyIdException;

    long count();

}