package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.TaskDto;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint extends IEndpoint {

    @NotNull
    @WebMethod
    Result clearAllTask(
            @WebParam(name = "session") @Nullable final SessionDto session
    );

    @NotNull
    @WebMethod
    List<TaskDto> getAllTaskList(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result createTask(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    ) throws AccessDeniedException;

    @NotNull
    @WebMethod
    Result clearTaskList(@WebParam(name = "session") @Nullable SessionDto session);

    @NotNull
    @WebMethod
    List<TaskDto> getTaskList(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @Nullable
    @WebMethod
    Result removeTaskById(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable String id
    ) throws AbstractException;


    @Nullable
    @WebMethod
    Result removeTaskByIndex(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable Integer index
    ) throws AbstractException;

    @Nullable
    @WebMethod
    Result removeTaskByName(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable String name
    ) throws AbstractException;

    @Nullable
    @WebMethod
    TaskDto getTaskById(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable String id
    ) throws AbstractException;


    @Nullable
    @WebMethod
    TaskDto getTaskByIndex(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable Integer index
    ) throws AbstractException;

    @Nullable
    @WebMethod
    TaskDto getTaskByName(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable String name
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result updateTaskById(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    ) throws AbstractException;


    @Nullable
    @WebMethod
    Result updateTaskByIndex(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable Integer index,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    ) throws AbstractException;

}