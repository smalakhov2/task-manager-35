package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint extends IEndpoint {

    @NotNull
    @WebMethod
    Result clearAllProject(
            @WebParam(name = "session") @Nullable SessionDto session
    );

    @NotNull
    @WebMethod
    List<ProjectDto> getAllProjectList(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result createProject(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    ) throws AccessDeniedException;

    @NotNull
    @WebMethod
    Result clearProjectList(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AccessDeniedException;

    @NotNull
    @WebMethod
    List<ProjectDto> getProjectList(
            @WebParam(name = "session") @Nullable SessionDto session
    ) throws AbstractException;

    @Nullable
    @WebMethod
    Result removeProjectById(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable String id
    ) throws AbstractException;


    @Nullable
    @WebMethod
    Result removeProjectByIndex(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable Integer index
    ) throws AbstractException;

    @Nullable
    @WebMethod
    Result removeProjectByName(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable String name
    ) throws AbstractException;

    @Nullable
    @WebMethod
    ProjectDto getProjectById(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable String id
    ) throws AbstractException;


    @Nullable
    @WebMethod
    ProjectDto getProjectByIndex(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable Integer index
    ) throws AbstractException;

    @Nullable
    @WebMethod
    ProjectDto getProjectByName(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable String name
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result updateProjectById(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    );


    @NotNull
    @WebMethod
    Result updateProjectByIndex(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "id") @Nullable Integer index,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    );

}