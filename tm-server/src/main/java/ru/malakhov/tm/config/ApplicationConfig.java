package ru.malakhov.tm.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("ru.malakhov.tm")
@Import({PersistenceConfig.class})
public class ApplicationConfig {
}