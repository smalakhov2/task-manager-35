package ru.malakhov.tm.exception.empty;

import ru.malakhov.tm.exception.AbstractException;

public final class EmptyProjectException extends AbstractException {

    public EmptyProjectException() {
        super("Error! Project is null...");
    }

}
