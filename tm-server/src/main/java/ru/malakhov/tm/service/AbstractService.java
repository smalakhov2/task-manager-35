package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.service.IService;
import ru.malakhov.tm.dto.AbstractEntityDto;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.repository.dto.AbstractDtoRepository;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class AbstractService<E extends AbstractEntityDto, R extends AbstractDtoRepository<E>> implements IService<E, R> {

    @Autowired
    protected R dtoRepository;

    protected AbstractService(@NotNull final R repository) {
        this.dtoRepository = repository;
    }

    @Override
    @Transactional
    public void save(@Nullable final E entity) {
        if (entity == null) return;
        dtoRepository.save(entity);
    }

    @Override
    @Transactional
    public void saveAll(@Nullable final Collection<E> entities) {
        if (entities == null) return;
        entities.removeIf(Objects::isNull);
        dtoRepository.saveAll(entities);
    }

    @Override
    @SafeVarargs
    @Transactional
    public final void saveAll(@Nullable final E... entities) {
        if (entities == null) return;
        @NotNull final List<E> collection =
                Arrays.stream(entities).filter(Objects::nonNull).collect(Collectors.toList());
        dtoRepository.saveAll(collection);
    }

    @NotNull
    @Override
    public List<E> findAllDto() {
        return dtoRepository.findAll();
    }

    @Nullable
    @Override
    public E findOneDtoById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return dtoRepository.findById(id).orElse(null);
    }

    @Override
    public boolean existById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return dtoRepository.existsById(id);
    }

    @Override
    public long count() {
        return dtoRepository.count();
    }

}