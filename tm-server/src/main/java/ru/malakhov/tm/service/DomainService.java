package ru.malakhov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.exception.empty.EmptyDomainException;
import ru.malakhov.tm.api.service.*;
import ru.malakhov.tm.dto.*;

import java.util.List;

@Service
@NoArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @Override
    public void loadData(@Nullable final Domain domain) throws EmptyDomainException {
        if (domain == null) throw new EmptyDomainException();
        userService.saveAll(domain.getUsers());
        projectService.saveAll(domain.getProjects());
        taskService.saveAll(domain.getTasks());
        sessionService.saveAll(domain.getSessions());
    }

    @Override
    public void saveData(@Nullable final Domain domain) throws EmptyDomainException {
        if (domain == null) throw new EmptyDomainException();
        @NotNull final List<ProjectDto> projects = projectService.findAllDto();
        domain.setProjects(projects);
        @NotNull final List<TaskDto> tasks = taskService.findAllDto();
        domain.setTasks(tasks);
        @NotNull final List<UserDto> users = userService.findAllDto();
        domain.setUsers(users);
        @NotNull final List<SessionDto> sessions = sessionService.findAllDto();
        domain.setSessions(sessions);
    }

}