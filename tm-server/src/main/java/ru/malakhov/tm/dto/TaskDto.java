package ru.malakhov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_task")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TaskDto extends AbstractEntityDto {

    public static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "name")
    private String name = "";

    @Nullable
    @Column(name = "description")
    private String description = "";

    @NotNull
    @Column(name = "user_id")
    private String userId;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    public TaskDto(@NotNull final String name) {
        this.name = name;
    }

    public TaskDto(@NotNull final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

    public TaskDto(
            @NotNull final String name,
            @Nullable final String description,
            @NotNull final String userId
    ) {
        this.name = name;
        this.description = description;
        this.userId = userId;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof TaskDto && super.equals(o)) {
            @NotNull final TaskDto task = (TaskDto) o;
            return Objects.equals(name, task.name)
                    && Objects.equals(description, task.description)
                    && Objects.equals(userId, task.userId)
                    && Objects.equals(projectId, task.projectId);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, userId, projectId);
    }

    public @NotNull String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    public @Nullable String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    public @NotNull String getUserId() {
        return userId;
    }

    public void setUserId(@NotNull String userId) {
        this.userId = userId;
    }

    public @Nullable String getProjectId() {
        return projectId;
    }

    public void setProjectId(@Nullable String projectId) {
        this.projectId = projectId;
    }

}