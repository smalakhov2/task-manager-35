package ru.malakhov.tm.repository.entity;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.entity.Session;

@Repository
public interface SessionRepository extends AbstractEntityRepository<Session> {

    @Query("SELECT e FROM Session e WHERE e.user.id=:userId")
    Iterable<Session> findByUserId(@Param("userId") String userId);

    @Modifying
    @Transactional
    @Query("DELETE Session e WHERE e.user.id=:userId")
    int deleteByUserId(@Param("userId") String userId);

}