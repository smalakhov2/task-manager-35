package ru.malakhov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.dto.ProjectDto;

import java.util.Optional;

@Repository
public interface ProjectDtoRepository extends AbstractDtoRepository<ProjectDto> {

    @NotNull
    Iterable<ProjectDto> findByUserId(String userId);

    @NotNull
    Iterable<ProjectDto> findByUserIdOrderByName(String userId);

    @NotNull
    Optional<ProjectDto> findByIdAndUserId(String id, String userId);

    @NotNull
    Optional<ProjectDto> findByUserIdAndName(String userId, String name);

}