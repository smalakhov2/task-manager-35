package ru.malakhov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.dto.TaskDto;

import java.util.Optional;

@Repository
public interface TaskDtoRepository extends AbstractDtoRepository<TaskDto> {

    @NotNull
    Iterable<TaskDto> findByUserId(String userId);

    @NotNull
    Iterable<TaskDto> findByUserIdOrderByName(String userId);

    @NotNull
    Optional<TaskDto> findByIdAndUserId(String id, String userId);

    @NotNull
    Optional<TaskDto> findByUserIdAndName(String userId, String name);

}