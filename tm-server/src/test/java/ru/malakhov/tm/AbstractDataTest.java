package ru.malakhov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import ru.malakhov.tm.config.ApplicationConfig;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.enumerated.Role;

public abstract class AbstractDataTest {

    @NotNull
    protected final AbstractApplicationContext context =
            new AnnotationConfigApplicationContext(ApplicationConfig.class);

    protected final String USER_PASSWORD = "test1";

    protected final String ADMIN_PASSWORD = "admin1";

    @NotNull
    protected final UserDto userDto = new UserDto("test1", "test1", "3@1.ru");

    @NotNull
    protected final UserDto adminDto = new UserDto("admin1", "admin1", "4@2.ru");

    @NotNull
    protected final UserDto unknownUserDto = new UserDto("unknown", "unknown", "2@2.ru");

    public AbstractDataTest() throws Exception {
        adminDto.setRole(Role.ADMIN);
    }

}