<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../../include/_header.jsp"/>

<h2>TASK EDIT</h2>

<form action="/task/edit/${task.id}" method="POST">

    <input type="hidden" name="id" value="${task.id}" />

    <p>
        <div>NAME:</div>
        <div><input type="text" name="name" value="${task.name}" /></div>
    </p>

    <p>
        <div>DESCRIPTION:</div>
        <div><input type="text" name="description" value="${task.description}" /></div>
    </p>

    <button type="submit">SAVE TASK</button>

</form>

<jsp:include page="../../include/_footer.jsp"/>

