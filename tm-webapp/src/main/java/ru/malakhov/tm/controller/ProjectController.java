package ru.malakhov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.malakhov.tm.api.service.dto.IProjectDtoService;
import ru.malakhov.tm.api.service.entity.IProjectService;
import ru.malakhov.tm.dto.ProjectDto;

@Controller
@RequestMapping("/project")
public class ProjectController {

    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @GetMapping("/delete/{id}")
    public ModelAndView delete(
            @Nullable @PathVariable("id") final String id
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/projects");
        if (id == null || id.isEmpty()) {
            modelAndView.setStatus(HttpStatus.BAD_REQUEST);
            return modelAndView;
        }
        projectService.deleteById(id);
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create() {
        projectDtoService.create();
        return new ModelAndView("redirect:/projects");
    }

    @GetMapping("/edit/{id}")
    public ModelAndView view(
            @Nullable @PathVariable("id") final String id
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("project_edit");
        if (id == null || id.isEmpty()) {
            modelAndView.setStatus(HttpStatus.BAD_REQUEST);
            return modelAndView;
        }
        @Nullable final ProjectDto project = projectDtoService.findOneById(id);
        if (project == null) {
            modelAndView.setStatus(HttpStatus.NOT_FOUND);
            return modelAndView;
        }
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    @PostMapping("/edit/{id}")
    public ModelAndView update(
            @ModelAttribute("project") ProjectDto projectDto,
            @NotNull BindingResult result
    ) {
        projectDtoService.save(projectDto);
        return new ModelAndView("redirect:/projects");
    }

}