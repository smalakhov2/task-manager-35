package ru.malakhov.tm.api.service.entity;

import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.repository.entity.ProjectRepository;

public interface IProjectService extends IService<Project, ProjectRepository> {
}