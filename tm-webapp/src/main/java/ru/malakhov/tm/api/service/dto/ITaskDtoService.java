package ru.malakhov.tm.api.service.dto;

import ru.malakhov.tm.dto.TaskDto;
import ru.malakhov.tm.repository.dto.TaskDtoRepository;

public interface ITaskDtoService extends IDtoService<TaskDto, TaskDtoRepository> {

    public void create();

}