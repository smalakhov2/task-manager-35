package ru.malakhov.tm.api.service.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.AbstractEntity;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.repository.entity.AbstractEntityRepository;

import java.util.Collection;
import java.util.List;

public interface IService<E extends AbstractEntity, R extends AbstractEntityRepository<E>> {

    void save(@Nullable final E entity);

    void saveAll(@Nullable final Collection<E> entities);

    void saveAll(@Nullable final E... entities);

    @NotNull
    List<E> findAll();

    @Nullable
    E findOneById(@Nullable final String id);

    boolean existById(@Nullable final String id) throws EmptyIdException;

    long count();

    void deleteById(@Nullable final String id);

    void delete(@Nullable final E entity);

    void deleteAll(@Nullable final Collection<E> entities);

    void deleteAll(@Nullable final E... entities);

    void deleteAll();

}