package ru.malakhov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.dto.AbstractEntityDto;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.repository.dto.AbstractDtoRepository;

import java.util.Collection;
import java.util.List;

public interface IDtoService<E extends AbstractEntityDto, R extends AbstractDtoRepository<E>> {

    void save(@Nullable final E entity);

    void saveAll(@Nullable final Collection<E> entities);

    @Transactional
    public void saveAll(@Nullable final E... entities);

    @NotNull
    List<E> findAll();

    @Nullable
    E findOneById(@NotNull final String id);

    boolean existById(@Nullable final String id) throws EmptyIdException;

    long count();

}