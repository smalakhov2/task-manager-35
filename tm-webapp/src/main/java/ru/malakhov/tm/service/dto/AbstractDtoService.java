package ru.malakhov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.service.dto.IDtoService;
import ru.malakhov.tm.dto.AbstractEntityDto;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.repository.dto.AbstractDtoRepository;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class AbstractDtoService<E extends AbstractEntityDto, R extends AbstractDtoRepository<E>> implements IDtoService<E, R> {

    protected R repository;

    protected AbstractDtoService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Override
    @Transactional
    public void save(@Nullable final E entity) {
        if (entity == null) return;
        repository.save(entity);
    }

    @Override
    @Transactional
    public void saveAll(@Nullable final Collection<E> entities) {
        if (entities == null || entities.isEmpty()) return;
        entities.removeIf(Objects::isNull);
        repository.saveAll(entities);
    }

    @Override
    @Transactional
    public void saveAll(@Nullable final E... entities) {
        if (entities == null || entities.length == 0) return;
        @NotNull final List<E> collection =
                Arrays.stream(entities).filter(Objects::nonNull).collect(Collectors.toList());
        repository.saveAll(collection);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public E findOneById(@Nullable final String id) {
        if (id == null) return null;
        return repository.findById(id).orElse(null);
    }

    @Override
    public boolean existById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.existsById(id);
    }

    @Override
    public long count() {
        return repository.count();
    }

}