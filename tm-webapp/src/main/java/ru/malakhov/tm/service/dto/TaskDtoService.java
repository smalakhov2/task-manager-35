package ru.malakhov.tm.service.dto;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.api.service.dto.ITaskDtoService;
import ru.malakhov.tm.dto.TaskDto;
import ru.malakhov.tm.repository.dto.TaskDtoRepository;

@Setter
@Service
public class TaskDtoService extends AbstractDtoService<TaskDto, TaskDtoRepository> implements ITaskDtoService {

    @Autowired
    public TaskDtoService(@NotNull final TaskDtoRepository taskDtoRepository) {
        super(taskDtoRepository);
    }

    public void create() {
        repository.save(new TaskDto("New task"));
    }

}