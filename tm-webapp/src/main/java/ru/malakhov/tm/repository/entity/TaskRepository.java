package ru.malakhov.tm.repository.entity;

import org.springframework.stereotype.Repository;
import ru.malakhov.tm.entity.Task;

@Repository
public interface TaskRepository extends AbstractEntityRepository<Task> {
}