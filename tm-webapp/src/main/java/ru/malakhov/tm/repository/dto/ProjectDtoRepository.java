package ru.malakhov.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.malakhov.tm.dto.ProjectDto;

@Repository
public interface ProjectDtoRepository extends AbstractDtoRepository<ProjectDto> {
}