package ru.malakhov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_project")
public class ProjectDto extends AbstractEntityDto {

    public static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "name")
    private String name = "";

    @Nullable
    @Column(name = "description")
    private String description = "";

    public ProjectDto(@NotNull final String name) {
        this.name = name;
    }

    public ProjectDto(@NotNull final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

    public ProjectDto(
            @NotNull final String name,
            @Nullable final String description,
            @NotNull final String userId
    ) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof ProjectDto && super.equals(o)) {
            @NotNull final ProjectDto project = (ProjectDto) o;
            return Objects.equals(name, project.name)
                    && Objects.equals(description, project.description);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }

}