package ru.malakhov.tm.event;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public final class ConsoleEvent extends AbstractEvent {

    public ConsoleEvent(@NotNull final String name) {
        super(name);
    }

}