package ru.malakhov.tm.listener.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.endpoint.SessionEndpoint;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.PropertyService;

@Component
public class LogoutListener extends AbstractListener {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout.";
    }

    @Override
    @EventListener(condition = "@logoutListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[LOGOUT]");
        @Nullable final SessionDto session = propertyService.getSession();
        @NotNull final Result result = sessionEndpoint.closeSession(session);
        if (result.isSuccess()) {
            System.out.println("[OK]");
            propertyService.setSession(null);
        } else System.out.println("[FAIL]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
