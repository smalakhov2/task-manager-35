package ru.malakhov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.component.IConsoleProvider;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.endpoint.UserEndpoint;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.PropertyService;

@Component
public class UserProfileUpdateListener extends AbstractListener {

    @NotNull
    @Autowired
    private IConsoleProvider consoleProvider;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String name() {
        return "update-profile";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update profile.";
    }

    @Override
    @EventListener(condition = "@userProfileUpdateListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws AbstractException_Exception {
        System.out.println("[UPDATE-PROFILE]");
        System.out.print("ENTER NEW EMAIL: ");
        @NotNull final String email = consoleProvider.nextLine();
        System.out.print("ENTER NEW FIRST NAME: ");
        @NotNull final String firstName = consoleProvider.nextLine();
        System.out.print("ENTER NEW LAST NAME: ");
        @NotNull final String lastName = consoleProvider.nextLine();
        System.out.print("ENTER NEW MIDDLE NAME: ");
        @NotNull final String middleName = consoleProvider.nextLine();
        @Nullable final SessionDto session = propertyService.getSession();
        @NotNull final Result result = userEndpoint.updateUser(
                session,
                email,
                firstName,
                lastName,
                middleName
        );
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}