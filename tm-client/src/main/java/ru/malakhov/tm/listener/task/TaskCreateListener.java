package ru.malakhov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.component.IConsoleProvider;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.endpoint.TaskEndpoint;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.PropertyService;

@Component
public class TaskCreateListener extends AbstractListener {

    @NotNull
    @Autowired
    private IConsoleProvider consoleProvider;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    @EventListener(condition = "@taskCreateListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[CREATE TASK]");
        System.out.print("ENTER TASK NAME: ");
        @NotNull final String name = consoleProvider.nextLine();
        System.out.print("ENTER TASK DESCRIPTION: ");
        @NotNull final String description = consoleProvider.nextLine();
        @Nullable final SessionDto session = propertyService.getSession();
        @NotNull final Result result = taskEndpoint.createTask(session, name, description);
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}
