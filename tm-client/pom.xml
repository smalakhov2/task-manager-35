<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>ru.malakhov.tm</groupId>
    <artifactId>tm-client</artifactId>
    <version>1.0.0</version>
    <name>tm-client</name>

    <developers>
        <developer>
            <id>smalakhov2</id>
            <name>Sergei Malakhov</name>
            <email>smalakhov@rencredit.ru</email>
        </developer>
    </developers>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.reflections</groupId>
            <artifactId>reflections</artifactId>
            <version>0.9.11</version>
        </dependency>

        <dependency>
            <groupId>org.jetbrains</groupId>
            <artifactId>annotations</artifactId>
            <version>20.1.0</version>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.16</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.eclipse.persistence</groupId>
            <artifactId>org.eclipse.persistence.moxy</artifactId>
            <version>2.6.0</version>
        </dependency>

        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.9.8</version>
        </dependency>

        <dependency>
            <groupId>com.fasterxml.jackson.dataformat</groupId>
            <artifactId>jackson-dataformat-xml</artifactId>
            <version>2.9.8</version>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.apache.cxf</groupId>
            <artifactId>cxf-codegen-plugin</artifactId>
            <version>3.4.2</version>
        </dependency>

        <dependency>
            <groupId>com.sun.xml.ws</groupId>
            <artifactId>jaxws-rt</artifactId>
            <version>2.1.4</version>
        </dependency>
        <dependency>
            <groupId>ru.malakhov.tm</groupId>
            <artifactId>tm-server</artifactId>
            <version>1.0.0</version>
            <scope>compile</scope>
        </dependency>

    </dependencies>

    <profiles>

        <profile>
            <id>SOAP</id>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.cxf</groupId>
                        <artifactId>cxf-codegen-plugin</artifactId>
                        <version>3.4.2</version>
                        <executions>
                            <execution>
                                <id>generate-sources</id>
                                <phase>generate-sources</phase>
                                <configuration>
                                    <defaultOptions>
                                        <bindingFiles>
                                            <bindingFile>${basedir}/src/main/jaxb/bindings.xml</bindingFile>
                                        </bindingFiles>
                                        <noAddressBinding>true</noAddressBinding>
                                    </defaultOptions>
                                    <wsdlOptions>
                                        <wsdlOption>
                                            <wsdl>http://localhost:80/AdminEndpoint?wsdl</wsdl>
                                        </wsdlOption>
                                        <wsdlOption>
                                            <wsdl>http://localhost:80/DataEndpoint?wsdl</wsdl>
                                        </wsdlOption>
                                        <wsdlOption>
                                            <wsdl>http://localhost:80/ProjectEndpoint?wsdl</wsdl>
                                        </wsdlOption>
                                        <wsdlOption>
                                            <wsdl>http://localhost:80/SessionEndpoint?wsdl</wsdl>
                                        </wsdlOption>
                                        <wsdlOption>
                                            <wsdl>http://localhost:80/TaskEndpoint?wsdl</wsdl>
                                        </wsdlOption>
                                        <wsdlOption>
                                            <wsdl>http://localhost:80/UserEndpoint?wsdl</wsdl>
                                        </wsdlOption>
                                    </wsdlOptions>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <id>UNIT</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <test.category>ru.malakhov.tm.category.UnitCategory</test.category>
            </properties>
        </profile>

        <profile>
            <id>INTEGRATION</id>
            <properties>
                <test.category>ru.malakhov.tm.category.IntegrationCategory</test.category>
            </properties>
        </profile>

    </profiles>

    <build>
        <finalName>tm-client</finalName>
        <plugins>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF-8</encoding>
                    <compilerArguments>
                        <bootclasspath>
                            ${java.home}/lib/rt.jar${path.separator}${java.home}/lib/javaws.jar${path.separator}${java.home}/lib/jce.jar
                        </bootclasspath>
                    </compilerArguments>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.22.2</version>
                <configuration>
                    <groups>${test.category}</groups>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>3.2.0</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                            <classpathPrefix>../lib/</classpathPrefix>
                            <mainClass>ru.malakhov.tm.App</mainClass>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>3.1.2</version>
                <executions>
                    <execution>
                        <id>copy</id>
                        <phase>install</phase>
                        <goals>
                            <goal>copy</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <artifactItems>
                        <artifactItem>
                            <groupId>ru.malakhov.tm</groupId>
                            <artifactId>tm-client</artifactId>
                            <version>1.0.0</version>
                            <type>jar</type>
                            <overWrite>true</overWrite>
                            <outputDirectory>${project.build.directory}/tm-client/bin</outputDirectory>
                            <destFileName>tm-client.jar</destFileName>
                        </artifactItem>
                    </artifactItems>
                    <overWriteReleases>false</overWriteReleases>
                    <overWriteSnapshots>true</overWriteSnapshots>
                </configuration>
            </plugin>

        </plugins>
    </build>
</project>